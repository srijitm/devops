from troposphere import Base64, FindInMap, GetAtt, Join, Output
from troposphere import Parameter, Ref, Tags, Template
from troposphere.cloudfront import Distribution, DistributionConfig
from troposphere.cloudfront import Origin, DefaultCacheBehavior
from troposphere.ec2 import PortRange
from troposphere.ec2 import Route
from troposphere.ec2 import SecurityGroupIngress
from troposphere.ec2 import RouteTable
from troposphere.ec2 import EIP, EIPAssociation, NetworkInterface, NetworkInterfaceProperty
from troposphere.ec2 import SecurityGroup, SecurityGroupRule
from troposphere.ec2 import SubnetRouteTableAssociation
from troposphere.ec2 import VPCGatewayAttachment
from troposphere.ec2 import Subnet
from troposphere.ec2 import InternetGateway, NatGateway
from troposphere.ec2 import Instance
from troposphere.ec2 import VPC
import troposphere.autoscaling as autoscaling
import troposphere.elasticloadbalancingv2 as elb
import troposphere.route53 as route53
from troposphere.cloudwatch import Alarm, MetricDimension
from troposphere.rds import DBInstance, DBSubnetGroup

import json
import yaml

# Load input json

with open('spec.json') as spec_file:    
    spec = json.load(spec_file)

t = Template()

t.add_description("""\
PlanPlus AWS Staging VPC\
""")

# tag variables
resource_tag = spec["project"]["tag"]
project_name = spec["project"]["name"]
environment_name = spec["project"]["env"]
ticket = spec["project"]["ticket"]
availability_zone_1 = spec["project"]["az1"]
availability_zone_2 = spec["project"]["az2"]

# params

vpcCidr_param = t.add_parameter(Parameter(
    "VpcCidr",
    Description="VPC CIDR",
    Default="10.0.0.0/16",
    Type="String",
    ))

natGatewayCidr_param = t.add_parameter(Parameter(
    "NatGatewayCidr",
    Description="Nat Gateway CIDR",
    Default="0.0.0.0/0",
    Type="String",
    ))

igwCidr_param = t.add_parameter(Parameter(
    "InternetGatewayCidr",
    Description="Internet Gateway CIDR",
    Default="0.0.0.0/0",
    Type="String",
    ))

publicSubnet01Cidr_param = t.add_parameter(Parameter(
    "PublicSubnet01Cidr",
    Description="PublicSubnet01 CIDR",
    Default="10.0.1.0/24",
    Type="String",
    ))

publicSubnet02Cidr_param = t.add_parameter(Parameter(
    "PublicSubnet02Cidr",
    Description="PublicSubnet02 CIDR",
    Default="10.0.2.0/24",
    Type="String",
    ))

privateWebSubnet01Cidr_param = t.add_parameter(Parameter(
    "privateWebSubnet01Cidr",
    Description="PrivateWebSubnet01 CIDR",
    Default="10.0.100.0/24",
    Type="String",
    ))

privateWebSubnet02Cidr_param = t.add_parameter(Parameter(
    "privateWebSubnet02Cidr",
    Description="PrivateWebSubnet02 CIDR",
    Default="10.0.101.0/24",
    Type="String",
    ))

privateDbSubnet01Cidr_param = t.add_parameter(Parameter(
    "privateDbSubnet01Cidr",
    Description="PrivateDbSubnet01 CIDR",
    Default="10.0.200.0/24",
    Type="String",
    ))

privateDbSubnet02Cidr_param = t.add_parameter(Parameter(
    "privateDbSubnet02Cidr",
    Description="PrivateDbSubnet02 CIDR",
    Default="10.0.201.0/24",
    Type="String",
    ))

availabilityZone01_param = t.add_parameter(Parameter(
    "AvailabilityZone01",
    Description="VPC AvailabilityZone01",
    Default=availability_zone_1,
    Type="String",
    ))

availabilityZone02_param = t.add_parameter(Parameter(
    "AvailabilityZone02",
    Description="VPC AvailabilityZone02",
    Default=availability_zone_2,
    Type="String",
    ))

tomcatPort_param = t.add_parameter(Parameter(
        "tomcatPort",
        Type="String",
        Default="80",
        Description="TCP/IP port of the web server",
    ))

dbPort_param = t.add_parameter(Parameter(
        "dbPort",
        Type="String",
        Default="3306",
        Description="TCP/IP port of the web server",
    ))

# Auto scaling group parameters

# Web Layer

web_asg_capacity = t.add_parameter(Parameter(
        "webAsgCapacity",
        Default="2",
        Type="Number",
        Description="Desired capcacity of AutoScalingGroup"
    ))
web_asg_min_size = t.add_parameter(Parameter(
        "webAsgMinSize",
        Default="2",
        Type="Number",
        Description="Minimum size of AutoScalingGroup"
    ))
web_asg_max_size = t.add_parameter(Parameter(
        "webAsgMaxSize",
        Default="5",
        Type="Number",
        Description="Maximum size of AutoScalingGroup"
    ))
web_asg_cooldown = t.add_parameter(Parameter(
        "webAsgCooldown",
        Default="360",
        Type="Number",
        Description="Cooldown before starting/stopping another instance"
    ))
web_asg_health_grace = t.add_parameter(Parameter(
        "webAsgHealthGrace",
        Default="360",
        Type="Number",
        Description="Wait before starting/stopping another instance"
    ))

# API Layer
api_asg_capacity = t.add_parameter(Parameter(
        "apiAsgCapacity",
        Default="2",
        Type="Number",
        Description="Desired capcacity of AutoScalingGroup"
    ))
api_asg_min_size = t.add_parameter(Parameter(
        "apiAsgMinSize",
        Default="2",
        Type="Number",
        Description="Minimum size of AutoScalingGroup"
    ))
api_asg_max_size = t.add_parameter(Parameter(
        "apiAsgMaxSize",
        Default="5",
        Type="Number",
        Description="Maximum size of AutoScalingGroup"
    ))
api_asg_cooldown = t.add_parameter(Parameter(
        "apiAsgCooldown",
        Default="360",
        Type="Number",
        Description="Cooldown before starting/stopping another instance"
    ))
api_asg_health_grace = t.add_parameter(Parameter(
        "apiAsgHealthGrace",
        Default="360",
        Type="Number",
        Description="Wait before starting/stopping another instance"
    ))

# ec2instancetype_param = t.add_parameter(Parameter(
#     "ec2InstanceType",
#     Description="EC2 InstanceType",
#     Default="t2.micro",
#     Type="String",
#     ))

# ec2imageid_param = t.add_parameter(Parameter(
#     "ec2ImageId",
#     Description="ec2 ImageId",
#     Default=ec2_ami,
#     Type="String",
#     ))


# VPC
VPC = t.add_resource(
    VPC(
        "VPC",
        EnableDnsSupport="true",
        CidrBlock=Ref(vpcCidr_param),
        EnableDnsHostnames="true",
        Tags=Tags(
            Name=Join("",[resource_tag,"-",environment_name,"-VPC"]),
            Environment=environment_name,
            Project=project_name,
            Ticket=ticket
        )
))

# Public Subnets
publicSubnet01 = t.add_resource(Subnet(
    "publicSubnet01",
    VpcId=Ref("VPC"),
    AvailabilityZone=Ref(availabilityZone01_param),
    CidrBlock=Ref(publicSubnet01Cidr_param),
    MapPublicIpOnLaunch=True,
    Tags=Tags(
        Name=Join("",[resource_tag,"-PublicSubnet-01"]),
        Environment=environment_name,
        Project=project_name,
        Ticket=ticket
    )
))

publicSubnet02 = t.add_resource(Subnet(
    "publicSubnet02",
    VpcId=Ref("VPC"),
    AvailabilityZone=Ref(availabilityZone02_param),
    CidrBlock=Ref(publicSubnet02Cidr_param),
    MapPublicIpOnLaunch=True,
    Tags=Tags(
        Name=Join("",[resource_tag,"-PublicSubnet-02"]),
        Environment=environment_name,
        Project=project_name,
        Ticket=ticket
    )
))

# Private Subnets
privateWebSubnet01 = t.add_resource(Subnet(
    "privateWebSubnet01",
    VpcId=Ref("VPC"),
    AvailabilityZone=Ref(availabilityZone01_param),
    CidrBlock=Ref(privateWebSubnet01Cidr_param),
    Tags=Tags(
        Name=Join("",[resource_tag,"-PrivateWebSubnet-01"]),
        Environment=environment_name,
        Project=project_name,
        Ticket=ticket
    )
))

privateWebSubnet02 = t.add_resource(Subnet(
    "privateWebSubnet02",
    VpcId=Ref("VPC"),
    AvailabilityZone=Ref(availabilityZone02_param),
    CidrBlock=Ref(privateWebSubnet02Cidr_param),
    Tags=Tags(
        Name=Join("",[resource_tag,"-PrivateWebSubnet-02"]),
        Environment=environment_name,
        Project=project_name,
        Ticket=ticket
    )
))

privateDbSubnet01 = t.add_resource(Subnet(
    "privateDbSubnet01",
    VpcId=Ref("VPC"),
    AvailabilityZone=Ref(availabilityZone01_param),
    CidrBlock=Ref(privateDbSubnet01Cidr_param),
    Tags=Tags(
        Name=Join("",[resource_tag,"-PrivateDbSubnet-01"]),
        Environment=environment_name,
        Project=project_name,
        Ticket=ticket
    )
))

privateDbSubnet02 = t.add_resource(Subnet(
    "privateDbSubnet02",
    VpcId=Ref("VPC"),
    AvailabilityZone=Ref(availabilityZone02_param),
    CidrBlock=Ref(privateDbSubnet02Cidr_param),
    Tags=Tags(
        Name=Join("",[resource_tag,"-PrivateDbSubnet-02"]),
        Environment=environment_name,
        Project=project_name,
        Ticket=ticket
    )
))

# Security Groups

# Generating Bastion security group rules
bas_security_group_rules = []
for ip in spec["pp_ips"]["ssh"]:
    rule=[SecurityGroupRule(
                IpProtocol='tcp',
                FromPort='22',
                ToPort='22',
                CidrIp=ip)]
    bas_security_group_rules.extend(rule)

for ip in spec["customer_ips"]["ssh"]:
    rule=[SecurityGroupRule(
                IpProtocol='tcp',
                FromPort='22',
                ToPort='22',
                CidrIp=ip)]
    bas_security_group_rules.extend(rule)


bastionSecurityGroup = t.add_resource(
    SecurityGroup(
        'bastionSecurityGroup',
        GroupDescription='Allow SSH connections from an approved list of IPs',
        SecurityGroupIngress=bas_security_group_rules,
        VpcId=Ref(VPC),
        Tags=Tags(
            Name=Join("",[resource_tag,"-BastionSecurityGroup"]),
            Environment=environment_name,
            Project=project_name,
            Ticket=ticket
        )
    ))

# Generating front end security group rules
fe_security_group_rules = [SecurityGroupRule(
                IpProtocol='tcp',
                FromPort='22',
                ToPort='22',
                SourceSecurityGroupId=Ref(bastionSecurityGroup))]

for ip in spec["pp_ips"]["http"]:
    rule=[SecurityGroupRule(
                IpProtocol='tcp',
                FromPort=Ref(tomcatPort_param),
                ToPort=Ref(tomcatPort_param),
                CidrIp=ip)]
    fe_security_group_rules.extend(rule)
    rule=[]

for ip in spec["customer_ips"]["http"]:
    rule=[SecurityGroupRule(
                IpProtocol='tcp',
                FromPort=Ref(tomcatPort_param),
                ToPort=Ref(tomcatPort_param),
                CidrIp=ip)]
    fe_security_group_rules.extend(rule)
    rule=[]

feSecurityGroup = t.add_resource(
    SecurityGroup(
        'feSecurityGroup',
        GroupDescription='Allow all necessary ports from the internet',
        SecurityGroupIngress=fe_security_group_rules,
        VpcId=Ref(VPC),
        Tags=Tags(
            Name=Join("",[resource_tag,"-feSecurityGroup"]),
            Environment=environment_name,
            Project=project_name,
            Ticket=ticket
        )
    ))

beSecurityGroup = t.add_resource(
    SecurityGroup(
        'beSecurityGroup',
        GroupDescription='Allow all necessary ports from PublicSubnet1, Allow conn to DB on 3306',
        SecurityGroupIngress=[
            SecurityGroupRule(
                IpProtocol='tcp',
                FromPort='22',
                ToPort='22',
                SourceSecurityGroupId=Ref(bastionSecurityGroup)),
            SecurityGroupRule(
                IpProtocol='tcp',
                FromPort=Ref(dbPort_param),
                ToPort=Ref(dbPort_param),
                SourceSecurityGroupId=Ref(feSecurityGroup))],
        VpcId=Ref(VPC),
        Tags=Tags(
            Name=Join("",[resource_tag,"-beSecurityGroup"]),
            Environment=environment_name,
            Project=project_name,
            Ticket=ticket
        )

    ))

# Internet Gateway
internetGateway = t.add_resource(
    InternetGateway(
        "InternetGateway",
        Tags=Tags(
            Name=Join("",[resource_tag,"-IGW"]),
            Environment=environment_name,
            Project=project_name,
            Ticket=ticket
    )
))

igwVpcAttachment = t.add_resource(
    VPCGatewayAttachment(
        'AttachInternetGatewayToVPC',
        VpcId=Ref(VPC),
        InternetGatewayId=Ref(internetGateway)
))

# Public Subnet Route Table
publicRouteTable = t.add_resource(
    RouteTable(
        "publicRouteTable",
        VpcId=Ref("VPC"),
        Tags=Tags(
            Name=Join("",[resource_tag,"-PublicRouteTable"]),
            Environment=environment_name,
            Project=project_name,
            Ticket=ticket
        )
))

publicSubnet01Association = t.add_resource(SubnetRouteTableAssociation(
    "publicSubnet01Association",
    SubnetId=Ref("publicSubnet01"),
    RouteTableId=Ref(publicRouteTable),
))

publicSubnet02Association = t.add_resource(SubnetRouteTableAssociation(
    "publicSubnet02Association",
    SubnetId=Ref("publicSubnet02"),
    RouteTableId=Ref(publicRouteTable),
))

igwRouteAttachment = t.add_resource(
    Route(
        'AttachInternetGatewayToPublicRouteTable',
        DestinationCidrBlock=Ref(igwCidr_param),
        GatewayId=Ref(internetGateway),
        RouteTableId=Ref(publicRouteTable)
))

# Private Subnet Route Table
natRouteTable = t.add_resource(
    RouteTable(
        "natRouteTable",
        VpcId=Ref("VPC"),
        Tags=Tags(
            Name=Join("",[resource_tag,"-NatRouteTable"]),
            Environment=environment_name,
            Project=project_name,
            Ticket=ticket
        )
))

natRouteWeb01Association = t.add_resource(
    SubnetRouteTableAssociation(
        "natRouteWeb01Association",
        SubnetId=Ref("privateWebSubnet01"),
        RouteTableId=Ref(natRouteTable),
))

natRouteWeb02Association = t.add_resource(
    SubnetRouteTableAssociation(
        "natRouteWeb02Association",
        SubnetId=Ref("privateWebSubnet02"),
        RouteTableId=Ref(natRouteTable),
))

natRouteDb01Association = t.add_resource(
    SubnetRouteTableAssociation(
        "natRouteDb01Association",
        SubnetId=Ref("privateDbSubnet01"),
        RouteTableId=Ref(natRouteTable),
))

natRouteDb02Association = t.add_resource(
    SubnetRouteTableAssociation(
        "natRouteDb02Association",
        SubnetId=Ref("privateDbSubnet02"),
        RouteTableId=Ref(natRouteTable),
))

natElasticIp = t.add_resource(
    EIP(
        "natElasticIp",
        Domain="vpc",
))

natGateway = t.add_resource(
    NatGateway(
        "natGateway",
        AllocationId=GetAtt(natElasticIp, 'AllocationId'),
        SubnetId=Ref("publicSubnet01")
    )
)

natRouteAttachment = t.add_resource(
    Route(
        'AttachNatGatewayToPrivateRouteTable',
        DestinationCidrBlock=Ref(natGatewayCidr_param),
        NatGatewayId=Ref(natGateway),
        RouteTableId=Ref(natRouteTable)
))

# Public Subnet Instances

# Bastion

if spec["bastion"]:

    bas_num_nodes = spec["bastion"]["num_nodes"]
    bas_name = spec["bastion"]["canonical_name"]
    bas_instance_type = spec["bastion"]["ec2InstanceType"]
    bas_ami_id = spec["bastion"]["ami_id"]

    # Defining network card to allocate elastic IP
    eth0 = t.add_resource(NetworkInterface(
        "eth0",
        Description="eth0",
        GroupSet=[Ref(bastionSecurityGroup)],
        SourceDestCheck=True,
        SubnetId=Ref(publicSubnet01),
        Tags=Tags(
            Name="Bastion NIC",
            Environment=environment_name,
            Project=project_name,
            Ticket=ticket,
            Interface="eth0",
        ),
    ))

    eipassoc1 = t.add_resource(EIPAssociation(
        "EIPAssoc1",
        NetworkInterfaceId=Ref(eth0),
        AllocationId="eipalloc-f1b5f2df",
        PrivateIpAddress=GetAtt("eth0", "PrimaryPrivateIpAddress"),
    ))

    for bas_node in xrange(1, int(bas_num_nodes)+1):
        t.add_resource(Instance(
            "bas"+str(bas_node).zfill(2) ,
            SourceDestCheck="false",
            ImageId=bas_ami_id,
            InstanceType=bas_instance_type,
            NetworkInterfaces=[
                NetworkInterfaceProperty(
                    NetworkInterfaceId=Ref(eth0),
                    DeviceIndex="0",
                ),
            ],
            Tags=Tags(
                Name=Join("",[resource_tag,"-",bas_name,"-",str(bas_node).zfill(2)]),
                Environment=environment_name,
                Project=project_name,
                Ticket=ticket
            )   
        ))

# Application ELB

web_alb_header = t.add_parameter(Parameter(
        "webAlbHeader",
        Default="planplusstaging.com",
        Type="CommaDelimitedList",
        Description="Host-header redirection to Web Layer"
    ))

web_alb_paths = t.add_parameter(Parameter(
        "webAlbPaths",
        Default="/",
        Type="CommaDelimitedList",
        Description="Path-patterns to Web Layer"
    ))

api_alb_header = t.add_parameter(Parameter(
        "apiAlbHeader",
        Default="api.planplusstaging.com",
        Type="CommaDelimitedList",
        Description="Host-header redirection to API Layer"
    ))

api_alb_paths = t.add_parameter(Parameter(
        "apiAlbPaths",
        Default="/",
        Type="CommaDelimitedList",
        Description="Path-patterns to API Layer"
    ))

web_alb_target_group = t.add_resource(elb.TargetGroup(
    "webAlbTargetGroup",
    HealthCheckPath="/",
    HealthCheckIntervalSeconds="30",
    HealthCheckProtocol="HTTP",
    HealthCheckTimeoutSeconds="10",
    HealthyThresholdCount="4",
    Matcher=elb.Matcher(
        HttpCode="200"),
    Name="WebLayerTargetGroup",
    Port=80,
    Protocol="HTTP",
    UnhealthyThresholdCount="3",
    VpcId=Ref(VPC),
    Tags=Tags(
        Name="WebLayerTargetGroup",
        Environment=environment_name,
        Project=project_name,
        Ticket=ticket
    )   
))

api_alb_target_group = t.add_resource(elb.TargetGroup(
    "apiAlbTargetGroup",
    HealthCheckPath="/",
    HealthCheckIntervalSeconds="30",
    HealthCheckProtocol="HTTP",
    HealthCheckTimeoutSeconds="10",
    HealthyThresholdCount="4",
    Matcher=elb.Matcher(
        HttpCode="200"),
    Name="ApiLayerTargetGroup",
    Port=80,
    Protocol="HTTP",
    UnhealthyThresholdCount="3",
    VpcId=Ref(VPC),
    Tags=Tags(
        Name="ApiLayerTargetGroup",
        Environment=environment_name,
        Project=project_name,
        Ticket=ticket
    )   
))

application_load_balancer = t.add_resource(elb.LoadBalancer(
    "applicationLoadBalancer",
    Name="PlanPlus-ALB",
    Scheme="internet-facing",
    Subnets=[Ref(privateWebSubnet01),Ref(privateWebSubnet02)],
    #SecurityGroups=Ref(feSecurityGroup)
))

alb_listener = t.add_resource(elb.Listener(
    "albListener",
    Port="80",
    Protocol="HTTP",
    LoadBalancerArn=Ref(application_load_balancer),
    DefaultActions=[elb.Action(
        Type="forward",
        TargetGroupArn=Ref(web_alb_target_group)
    )]
))

alb_web_listener_rule = t.add_resource(elb.ListenerRule(
    "albWebListenerRule",
    ListenerArn=Ref(alb_listener),
    Conditions=[elb.Condition(
        Field="host-header",
        Values=Ref(web_alb_header)
        )
    ],
    Actions=[elb.Action(
        Type="forward",
        TargetGroupArn=Ref(web_alb_target_group)
    )],
    Priority="1"
))

alb_api_listener_rule = t.add_resource(elb.ListenerRule(
    "albApiListenerRule",
    ListenerArn=Ref(alb_listener),
    Conditions=[elb.Condition(
        Field="host-header",
        Values=Ref(api_alb_header)
        )
    ],
    Actions=[elb.Action(
        Type="forward",
        TargetGroupArn=Ref(api_alb_target_group)
    )],
    Priority="2"
))

# Auto Scaling Groups

# Web Layer
# Launchconfiguration
web_name = spec["web"]["canonical_name"]
web_instance_type = spec["web"]["ec2InstanceType"]
web_ami_id = spec["web"]["ami_id"]

webEC2LaunchConfiguration = t.add_resource(autoscaling.LaunchConfiguration(
    "webEC2LaunchConfiguration",
    ImageId=web_ami_id,
    InstanceType=web_instance_type,
    AssociatePublicIpAddress=False,
    SecurityGroups=[Ref(feSecurityGroup)],
))

webASG = t.add_resource(autoscaling.AutoScalingGroup(
        "webAutoScalingGroup",
        DesiredCapacity=Ref(web_asg_capacity),
        Tags=autoscaling.Tags(
            Name=Join("",[resource_tag,"-",web_name]),
            Environment=environment_name,
            Project=project_name,
            Ticket=ticket
        ),
        VPCZoneIdentifier=[Ref(privateWebSubnet01),Ref(privateWebSubnet02)],
        TargetGroupARNs=[Ref(web_alb_target_group)],
        MinSize=Ref(web_asg_min_size),
        MaxSize=Ref(web_asg_max_size),
        Cooldown=Ref(web_asg_cooldown),
        LaunchConfigurationName=Ref(webEC2LaunchConfiguration),
        HealthCheckGracePeriod=Ref(web_asg_health_grace),
        HealthCheckType="EC2",
    ))

webAsgScalingUp = t.add_resource(autoscaling.ScalingPolicy(
    "webAsgScalingUp",
    AdjustmentType="ChangeInCapacity",
    AutoScalingGroupName=Ref(webASG),
    Cooldown="360",
    ScalingAdjustment="1",
))

webAsgScalingDown = t.add_resource(autoscaling.ScalingPolicy(
    "webAsgScalingDown",
    AdjustmentType="ChangeInCapacity",
    AutoScalingGroupName=Ref(webASG),
    Cooldown="360",
    ScalingAdjustment="-1",
))

webHighCpuUsageAlarm = t.add_resource(Alarm(
    "webHighCpuUsageAlarm",
    AlarmDescription="Alarm if cpu usage above 80 for 25min",
    Namespace="AWS/EC2",
    MetricName="CPUUtilization",
    Statistic="Average",
    Period="1500",
    EvaluationPeriods="1",
    Threshold="85",
    ComparisonOperator="GreaterThanThreshold",
    AlarmActions=[Ref(webAsgScalingUp)]
))

webLowCpuUsageAlarm = t.add_resource(Alarm(
    "webLowCpuUsageAlarm",
    AlarmDescription="Alarm if cpu usage below 80 for 25min",
    Namespace="AWS/EC2",
    MetricName="CPUUtilization",
    Statistic="Average",
    Period="1500",
    EvaluationPeriods="1",
    Threshold="60",
    ComparisonOperator="LessThanThreshold",
    AlarmActions=[Ref(webAsgScalingDown)]
))

# API Layer
# Launchconfiguration

api_name = spec["api"]["canonical_name"]
api_instance_type = spec["api"]["ec2InstanceType"]
api_ami_id = spec["api"]["ami_id"]

apiEC2LaunchConfiguration = t.add_resource(autoscaling.LaunchConfiguration(
    "apiEC2LaunchConfiguration",
    ImageId=api_ami_id,
    #InstanceId=Ref(web01),
    InstanceType=api_instance_type,
    AssociatePublicIpAddress=False,
    SecurityGroups=[Ref(feSecurityGroup)],
))

apiASG = t.add_resource(autoscaling.AutoScalingGroup(
        "apiAutoScalingGroup",
        DesiredCapacity=Ref(api_asg_capacity),
        Tags=autoscaling.Tags(
            Name=Join("",[resource_tag,"-",api_name]),
            Environment=environment_name,
            Project=project_name,
            Ticket=ticket
        ),
        VPCZoneIdentifier=[Ref(privateWebSubnet01),Ref(privateWebSubnet02)],
        TargetGroupARNs=[Ref(api_alb_target_group)],
        MinSize=Ref(api_asg_min_size),
        MaxSize=Ref(api_asg_max_size),
        Cooldown=Ref(api_asg_cooldown),
        LaunchConfigurationName=Ref(apiEC2LaunchConfiguration),
        HealthCheckGracePeriod=Ref(api_asg_health_grace),
        HealthCheckType="EC2",
    ))

apiAsgScalingUp = t.add_resource(autoscaling.ScalingPolicy(
    "apiAsgScalingUp",
    AdjustmentType="ChangeInCapacity",
    AutoScalingGroupName=Ref(apiASG),
    Cooldown="360",
    ScalingAdjustment="1",
))

apiAsgScalingDown = t.add_resource(autoscaling.ScalingPolicy(
    "apiAsgScalingDown",
    AdjustmentType="ChangeInCapacity",
    AutoScalingGroupName=Ref(apiASG),
    Cooldown="360",
    ScalingAdjustment="-1",
))

apiHighCpuUsageAlarm = t.add_resource(Alarm(
    "apiHighCpuUsageAlarm",
    AlarmDescription="Alarm if cpu usage above 80 for 25min",
    Namespace="AWS/EC2",
    MetricName="CPUUtilization",
    Statistic="Average",
    Period="1500",
    EvaluationPeriods="1",
    Threshold="85",
    ComparisonOperator="GreaterThanThreshold",
    AlarmActions=[Ref(apiAsgScalingUp)]
))

apiLowCpuUsageAlarm = t.add_resource(Alarm(
    "apiLowCpuUsageAlarm",
    AlarmDescription="Alarm if cpu usage below 80 for 25min",
    Namespace="AWS/EC2",
    MetricName="CPUUtilization",
    Statistic="Average",
    Period="1500",
    EvaluationPeriods="1",
    Threshold="60",
    ComparisonOperator="LessThanThreshold",
    AlarmActions=[Ref(apiAsgScalingDown)]
))

privateDbSubnetGroup = t.add_resource(DBSubnetGroup(
    "privateDbSubnetGroup",
    DBSubnetGroupDescription="Subnets available for the RDS DB Instances",
    SubnetIds=[Ref(privateDbSubnet01), Ref(privateDbSubnet02)]
))

if spec["rds"]:

    rds_num_nodes = spec["rds"]["num_nodes"]
    rds_name = spec["rds"]["canonical_name"]
    rds_instance_type = spec["rds"]["ec2InstanceType"]
    rds_allocation_size = spec["rds"]["allocation_size"]

    for rds_node in xrange(1, int(rds_num_nodes)+1):
        rds_node = t.add_resource(DBInstance(
            "rds"+str(rds_node).zfill(2),
            DBName="PlanPlus",
            AllocatedStorage=rds_allocation_size,
            DBInstanceClass=rds_instance_type,
            Engine="MySQL",
            EngineVersion="5.7.19",
            AutoMinorVersionUpgrade="true",
            MasterUsername=Join("",["rdsgroup",str(rds_node),"master"]),
            MasterUserPassword="tq-pW2*fJyhL+Z3F",
            StorageEncrypted="true",
            DBSubnetGroupName=Ref(privateDbSubnetGroup),
            VPCSecurityGroups=[Ref(beSecurityGroup)],
            PubliclyAccessible="false",
            MultiAZ="true",
            BackupRetentionPeriod="35",
            Tags=Tags(
                Name=Join("",[resource_tag,"-",rds_name,"-",str(rds_node).zfill(2)]),
                Environment=environment_name,
                Project=project_name,
                Ticket=ticket
                )   

        ))

print(t.to_json())